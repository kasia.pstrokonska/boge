import styled from 'styled-components'

export const Title = styled.h2`
  font-family: "Helvetica Neue LT W02_57 Cond",Helvetica,Arial;
  letter-spacing: 0.025em;
  font-weight: 700;
  color: #0071b9;
  font-size: 1.5em;`

export const Paragraph = styled.p`
  font-family: Arial,Helvetica;
  color: #404040;
  font-size: 0.875em;
`
export const Button = styled.button`
  font-family: Arial,Helvetica;
  font-weight: 400;
  cursor: pointer;
  border-radius: 0.3125em;
  border: none;
  color: #fff;
  padding: 1.125em 2em;
  font-size: 0.875 ;
  background: linear-gradient(#8ed026,#83b11f);
`

export const Bold = styled.span`
  font-weight: 700;
`
