import { Title, Paragraph, Button, Bold } from './SmallCompressorsAdvantagesDescriptionStyles'

const SmallCompressorsAdvantagesDescription = ({ data }) => {
  return (
    <div>
      <Title>{data.title}</Title>
      <Paragraph>
        {data.content.light1}
        <Bold>{data.content.bold1}</Bold>
        {data.content.light2}
        <Bold>{data.content.bold2}</Bold>
        {data.content.light3}
      </Paragraph>
      <Button>{data.button_text}</Button>
    </div>
  )
}

export default SmallCompressorsAdvantagesDescription
