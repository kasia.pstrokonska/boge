import './App.css';
import SmallCompressorsAdvantagesDescription from './components/SmallCompressorsAdvantagesDescription'

const SmallCompressorsAdvantagesDescriptionData = {
  title: "BAFA-forderung - Nutzen Sie Ihren Vorteil",
  content: {
    light1: "Die Bundesregierung fördert Investitionen in ",
    bold1: "energieeffiziente Drucklufttechnologien ",
    light2: "mit bis zu ",
    bold2: "40 % des Netto-Investitionsvolumens. ",
    light3: "Der BAFA Antrag muss vor Bestellung erstellt werden und eine Eingangsbestätigung der BAFA vorliegen. Lassen Sie sich gerne von uns beraten, die Beantragung der Investitionszuschüsse ist denkbar einfach und wird natürlich von uns unterstützt."
  }, 
  button_text: "Jetzt Kontakt Aufnehmen"

}

function App() {
  return (
    <div className="App">
      <SmallCompressorsAdvantagesDescription data = {SmallCompressorsAdvantagesDescriptionData} />
    </div>
  );
}

export default App;
